import { Request, Response, NextFunction } from "express";

export async function ServerMiddleware(req: Request, res: Response, next: NextFunction)
{
    try
    {
        if( req.headers.authorization === null || req.headers.authorization === undefined || req.headers.authorization === "" )
            throw new Error( "No se ajuntaron las credenciales necesarias");

        next();
    }
    catch ( err:any )
    {
        res.status( 418 ).json( { error: true, type:"Error authorization", response: err.message } );
    }
}