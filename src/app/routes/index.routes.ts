import  cors from "cors";
import { Application } from "express";
import nocache from "nocache";

//Routes
import { PruebasRoutes } from "../rest/pruebas/pruebas.routes";

//Middlewares
import { ServerMiddleware } from "../middlewares/server.middlewares";

//Opciones de configuración de CORS
const options: cors.CorsOptions =
    {
        allowedHeaders:
            [
                'Origin',
                'X-Requested-With',
                'Content-Type',
                'Accept',
                'X-Access-Token',
            ],
        credentials: true,
        methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
        preflightContinue: false,
    };

export class IndexRoutes
{
    private url:string = "/pruebaliverpool"
    private ruta:any   = [];

    constructor()
    {
        this.crear_rutas();
    }

    public crear_rutas()
    {
        Object.values( PruebasRoutes ).forEach( ( url:any )=>
        {
            this.ruta.push( { path: this.url, router: url } );
        });
    }

    public rutas( app: Application )
    {
        this.ruta.forEach( ( url:any )=>
        {
            app.use( url.path, [ cors( options ), ServerMiddleware ], url.router );
        });
    }

    public Cors( app:Application )
    {
        const allowCrossDomain = ( req: any, res: any, next: any ) =>
        {
            res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
            res.header("Access-Control-Allow-Headers", "Content-Type");

            next();
        };

        app.use("*", allowCrossDomain);
        app.use( nocache );//Se inhabilita el uso de CACHE
    }
}