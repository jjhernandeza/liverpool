import { Request, Response } from "express";
import { ProductsModels } from "../models/servicios.models";


const modelo: ProductsModels = new ProductsModels();

export async function search( req:Request, res:Response )
{
    try
    {
        const brand:any = ( req.query.brand === null || req.query.brand === undefined || req.query.brand === "null"  ) ? null : req.query.brand;

        const productos:any = await modelo.consultarProductos( brand ).then( (r:any) => { return r; } ).catch( (err:any) => { return err; } );

        if(productos.error )
            throw new Error( "Error al generar la consulta solicitda");


        console.log( typeof productos.product  )
        res.status( 200 ).json( { error: false, type:"success", response: JSON.parse( productos.product ) } );

    }
    catch ( err:any )
    {
        res.status( 418 ).json( { error: true, type:"Error search", response: err.message } );
    }
}