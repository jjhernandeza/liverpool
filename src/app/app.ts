import { Server } from "./server/server";
import os from 'os'

import {environments} from "../enviroments/enviroments";

class App
{
    constructor()
    {
        let host: string = os.hostname();
        const myServer = new Server();
        myServer.InitializeServer();

        //API
        const table =
            [
                {
                    Nombre: 'PRUEBALIVERPOOL.API',
                    Host: host,
                    Puerto: environments.PORT,
                    Production: environments.production,
                }
            ];

        console.table( table );
    }
}
new App();