//Inicializa el servidor Express
import express, { Application } from 'express';
import compression from "compression";
import userAgent from 'express-useragent'
import requestIp from 'request-ip';

import http from 'http';
import https from 'https';
import colors from 'colors';
import path from 'path';
import { readFileSync } from "fs";
import { IndexRoutes } from '../routes/index.routes';
import cors from "cors";
import * as moment from 'moment-timezone';

import 'moment/locale/es-us';
import { environments } from "../../enviroments/enviroments";

const fileUpload = require('express-fileupload')

export class Server
{
    web_server: Application;
    constructor()
    {
        this.WebService();
    }

    WebService()
    {
        this.web_server = express();
        this.middlewares();
        this.routes();
        this.LoadIpConfiguration();
    }

    public InitializeServer()
    {
        try
        {
            let SERVER = ( environments.enableSSL ) ? https.createServer({ key: readFileSync(environments.SSLConfig.key), cert: readFileSync(environments.SSLConfig.cert) }, this.web_server) : http.createServer(this.web_server);
            SERVER.listen( environments.PORT, () =>
            {
                if (environments.log)
                    console.log(colors.green('Servicio corriendo: http://localhost:' + environments.PORT.toString() + '/pruebaliverpool/') );
            });

        } catch (e) {
            console.log(colors.red(<string>e));
        }
    }

    //Carga todos los complementos o middlewares de una lista
    public middlewares()
    {
        // this.web_server.use(MIDDLEWARES);
        //si no se configura las peticiones cruzadas fallaran, error cross access
        this.web_server.use(cors());

        this.web_server.use(express.json({ limit: '15mb' }));
        this.web_server.use(express.urlencoded({ extended: true }));
        this.web_server.use(compression());
        this.web_server.use(userAgent.express());
        this.web_server.use(requestIp.mw());
        this.web_server.use(express.static('public', { etag: false } ) );
        this.web_server.use(
            fileUpload
            ({
                useTempFiles: true,
                tempFileDir: '/tmp/',
                fileSize: 70 * 1024 * 1024
            })
        );
    }

    //Carga todas las rutas de express
    public routes()
    {
        const routes:IndexRoutes = new IndexRoutes();
        routes.rutas( this.web_server );
        routes.Cors( this.web_server );
    }

    //Permite leer la propiedad X-Forwarded-For para las ip publicas
    public  LoadIpConfiguration() {
        this.web_server.set('trust proxy', true)
    }
}