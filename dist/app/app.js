"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const server_1 = require("./server/server");
const os_1 = __importDefault(require("os"));
const enviroments_1 = require("../enviroments/enviroments");
class App {
    constructor() {
        let host = os_1.default.hostname();
        const myServer = new server_1.Server();
        myServer.InitializeServer();
        //API
        const table = [
            {
                Nombre: 'PRUEBALIVERPOOL.API',
                Host: host,
                Puerto: enviroments_1.environments.PORT,
                Production: enviroments_1.environments.production,
            }
        ];
        console.table(table);
    }
}
new App();
