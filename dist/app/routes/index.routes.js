"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IndexRoutes = void 0;
const cors_1 = __importDefault(require("cors"));
const nocache_1 = __importDefault(require("nocache"));
//Routes
const pruebas_routes_1 = require("../rest/pruebas/pruebas.routes");
//Middlewares
const server_middlewares_1 = require("../middlewares/server.middlewares");
//Opciones de configuración de CORS
const options = {
    allowedHeaders: [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'X-Access-Token',
    ],
    credentials: true,
    methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
};
class IndexRoutes {
    constructor() {
        this.url = "/pruebaliverpool";
        this.ruta = [];
        this.crear_rutas();
    }
    crear_rutas() {
        Object.values(pruebas_routes_1.PruebasRoutes).forEach((url) => {
            this.ruta.push({ path: this.url, router: url });
        });
    }
    rutas(app) {
        this.ruta.forEach((url) => {
            app.use(url.path, [(0, cors_1.default)(options), server_middlewares_1.ServerMiddleware], url.router);
        });
    }
    Cors(app) {
        const allowCrossDomain = (req, res, next) => {
            res.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
            res.header("Access-Control-Allow-Headers", "Content-Type");
            next();
        };
        app.use("*", allowCrossDomain);
        app.use(nocache_1.default); //Se inhabilita el uso de CACHE
    }
}
exports.IndexRoutes = IndexRoutes;
