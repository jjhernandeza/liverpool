"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServerMiddleware = void 0;
function ServerMiddleware(req, res, next) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            if (req.headers.authorization === null || req.headers.authorization === undefined || req.headers.authorization === "")
                throw new Error("No se ajuntaron las credenciales necesarias");
            next();
        }
        catch (err) {
            res.status(418).json({ error: true, type: "Error authorization", response: err.message });
        }
    });
}
exports.ServerMiddleware = ServerMiddleware;
