"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Server = void 0;
//Inicializa el servidor Express
const express_1 = __importDefault(require("express"));
const compression_1 = __importDefault(require("compression"));
const express_useragent_1 = __importDefault(require("express-useragent"));
const request_ip_1 = __importDefault(require("request-ip"));
const http_1 = __importDefault(require("http"));
const https_1 = __importDefault(require("https"));
const colors_1 = __importDefault(require("colors"));
const fs_1 = require("fs");
const index_routes_1 = require("../routes/index.routes");
const cors_1 = __importDefault(require("cors"));
require("moment/locale/es-us");
const enviroments_1 = require("../../enviroments/enviroments");
const fileUpload = require('express-fileupload');
class Server {
    constructor() {
        this.WebService();
    }
    WebService() {
        this.web_server = (0, express_1.default)();
        this.middlewares();
        this.routes();
        this.LoadIpConfiguration();
    }
    InitializeServer() {
        try {
            let SERVER = (enviroments_1.environments.enableSSL) ? https_1.default.createServer({ key: (0, fs_1.readFileSync)(enviroments_1.environments.SSLConfig.key), cert: (0, fs_1.readFileSync)(enviroments_1.environments.SSLConfig.cert) }, this.web_server) : http_1.default.createServer(this.web_server);
            SERVER.listen(enviroments_1.environments.PORT, () => {
                if (enviroments_1.environments.log)
                    console.log(colors_1.default.green('Servicio corriendo: http://localhost:' + enviroments_1.environments.PORT.toString() + '/pruebaliverpool/'));
            });
        }
        catch (e) {
            console.log(colors_1.default.red(e));
        }
    }
    //Carga todos los complementos o middlewares de una lista
    middlewares() {
        // this.web_server.use(MIDDLEWARES);
        //si no se configura las peticiones cruzadas fallaran, error cross access
        this.web_server.use((0, cors_1.default)());
        this.web_server.use(express_1.default.json({ limit: '15mb' }));
        this.web_server.use(express_1.default.urlencoded({ extended: true }));
        this.web_server.use((0, compression_1.default)());
        this.web_server.use(express_useragent_1.default.express());
        this.web_server.use(request_ip_1.default.mw());
        this.web_server.use(express_1.default.static('public', { etag: false }));
        this.web_server.use(fileUpload({
            useTempFiles: true,
            tempFileDir: '/tmp/',
            fileSize: 70 * 1024 * 1024
        }));
    }
    //Carga todas las rutas de express
    routes() {
        const routes = new index_routes_1.IndexRoutes();
        routes.rutas(this.web_server);
        routes.Cors(this.web_server);
    }
    //Permite leer la propiedad X-Forwarded-For para las ip publicas
    LoadIpConfiguration() {
        this.web_server.set('trust proxy', true);
    }
}
exports.Server = Server;
