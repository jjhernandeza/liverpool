"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.search = void 0;
const servicios_models_1 = require("../models/servicios.models");
const modelo = new servicios_models_1.ProductsModels();
function search(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const brand = (req.query.brand === null || req.query.brand === undefined || req.query.brand === "null") ? null : req.query.brand;
            const productos = yield modelo.consultarProductos(brand).then((r) => { return r; }).catch((err) => { return err; });
            if (productos.error)
                throw new Error("Error al generar la consulta solicitda");
            console.log(typeof productos.product);
            res.status(200).json({ error: false, type: "success", response: JSON.parse(productos.product) });
        }
        catch (err) {
            res.status(418).json({ error: true, type: "Error search", response: err.message });
        }
    });
}
exports.search = search;
