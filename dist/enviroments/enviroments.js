"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.environments = void 0;
exports.environments = {
    production: false,
    Databases: {},
    log: true,
    PORT: 3054,
    enableSSL: false,
    SSLConfig: {
        cert: '',
        key: ''
    },
    jwtConfig: {
        exp: undefined
    },
    mailConfig: {
        nodemailer: {
            pool: true,
            maxConnections: 10,
            host: "",
            port: 25,
            secure: false,
            auth: { user: "", pass: "" },
            tls: {
                // do not fail on invalid certs
                ciphers: 'SSLv3',
                rejectUnauthorized: false
            }
        }
    },
};
